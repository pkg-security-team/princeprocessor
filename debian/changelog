princeprocessor (0.22-6) unstable; urgency=medium

  * Add support for loong64 architecture. Thanks to Zhang Na for the reminder.
    Closes: #1058907

 -- Sascha Steinbiss <satta@debian.org>  Wed, 20 Dec 2023 06:20:41 +0100

princeprocessor (0.22-5) unstable; urgency=medium

  * Enable building multiple times in a row.
    Closes: #1045971

 -- Sascha Steinbiss <satta@debian.org>  Mon, 11 Dec 2023 14:46:18 +0100

princeprocessor (0.22-4) unstable; urgency=medium

  * Add support for riscv64.
    Thanks to Bo Yu for the patch.
    Closes: #1017037
  * Remove d/NEWS which is not longer NEWS anymore.
  * Bump Standards-Version.
  * Update debian/ directory copyright date in d/copyright.

 -- Sascha Steinbiss <satta@debian.org>  Mon, 05 Dec 2022 00:29:29 +0100

princeprocessor (0.22-3) unstable; urgency=medium

  * Fix watchfile.
  * Bump Standards-Version.

 -- Sascha Steinbiss <satta@debian.org>  Wed, 03 Nov 2021 22:01:26 +0100

princeprocessor (0.22-2) unstable; urgency=medium

  * Team Upload

  [ Samuel Henrique ]
  * Bump DH to 13
  * Bump Standards-Version to 4.5.1
  * Bump d/watch to v4
  * Add salsa-ci.yml
  * Configure git-buildpackage for Debian
  * d/control: Add 'Rules-Requires-Root: no'
  * d/s/include-binaries: Remove file, not needed

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit,
    Repository, Repository-Browse.

 -- Samuel Henrique <samueloph@debian.org>  Tue, 02 Feb 2021 00:20:45 +0000

princeprocessor (0.22-1) unstable; urgency=medium

  [ Raphaël Hertzog ]
  * Update team maintainer address to Debian Security Tools.
  * Update Vcs-Git and Vcs-Browser for the move to salsa.debian.org.

  [ Sascha Steinbiss ]
  * New upstream version 0.22.
  * Remove spelling patch applied by upstream.
  * Move lintian overrides to recommended location.
  * Use debhelper 11.
  * Bump Standards-Version.

 -- Sascha Steinbiss <satta@debian.org>  Fri, 13 Jul 2018 10:53:28 +0200

princeprocessor (0.21-3) unstable; urgency=medium

  * Rename executable to avoid name clash with /usr/bin/pp64 from
    polylib-utils. Thanks to Andreas Beckmann for the hint.
    Closes: #840826

 -- Sascha Steinbiss <satta@debian.org>  Sat, 15 Oct 2016 11:53:06 +0000

princeprocessor (0.21-2) unstable; urgency=medium

  * Restrict architectures to 64-bit ones.
  * Transfer maintainership to pkg-security team.

 -- Sascha Steinbiss <satta@debian.org>  Fri, 14 Oct 2016 09:46:38 +0000

princeprocessor (0.21-1) unstable; urgency=low

  * Initial packaging (Closes: #834438)

 -- Sascha Steinbiss <satta@debian.org>  Tue, 16 Aug 2016 05:49:21 +0000
